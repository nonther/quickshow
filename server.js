// ---------- DEPENDENCIES ---------- \\
const fs = require('fs');
const express = require('express');
const session = require('express-session')
const parser = require('body-parser');
const sass = require('node-sass');
// const jwt = require('jsonwebtoken');
const couchbase = require('couchbase');
const socket = require('socket.io');
const minify = require('express-minify');
const uglify = require('uglify-js');
var passport = require('passport')
  , TwitterStrategy = require('passport-twitter').Strategy, FacebookStrategy = require('passport-facebook').Strategy;
// ---------- Encryption ---------- \\
// var privateKEY  = fs.readFileSync('./jwtRS256.key', 'utf8');
// var publicKEY  = fs.readFileSync('./jwtRS256.key.pub', 'utf8');
// function JWTsend(payload) {
//   var i  = 'nonther';          // Issuer
//   var s  = 'redacted@email.com';        // Subject
//   var a  = 'http://localhost:8000'; // Audience
//   var signOptions = {
//    issuer:  i,
//    subject:  s,
//    audience:  a,
//    expiresIn:  "12h",
//    algorithm:  "RS256"
//   };
//   return jwt.sign(payload, privateKEY, signOptions);
// }
// function JWTdecrypt(token) {
//   var verifyOptions = {
//    issuer:  i,
//    subject:  s,
//    audience:  a,
//    expiresIn:  "12h",
//    algorithms:  ["RS256"]
//   };
//   return jwt.verify(token, publicKEY, verifyOptions);
// }

// ---------- VARIABLES ---------- \\
const saltRounds = 10
const tw = {
  consumerKey: '', // get your Consumer API Key at https://developer.twitter.com
  consumerSecret: '', // get your Consumer API Secret Key at https://developer.twitter.com
  callbackUrl: '', // Twitter only allows the same callback URL that you set on your Twitter app
  token: '', // get your Access Token at https://developer.twitter.com
  tokenSecret: '' // get your Access Token Secret at https://developer.twitter.com
}

const fb = {
  appid: "", // Get your AppID from https://developers.facebook.com
  appsecret: "" // Get your AppSecret from https://developers.facebook.com
}
// ----------: Production :----------\\
// const csses = {};
// const scsses = ["editor.scss"];

// ---------- EXPRESS:CONFIG ---------- \\
const app = express();
app.use(session({
  secret: 'My dolphin is not working', // Elon musk Meme Review reference, set it to anything you like
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}))
app.use(passport.initialize())
app.set('view engine', 'pug');
app.use(minify({
  uglifyJsModule: uglify
}));
app.use(express.static("public"));
var server = app.listen(8000);
// ---------- SOCKET.IO ---------- \\
const io = socket(server);
io.on('connection', conn => {
  var socketwise = conn.handshake.query.socketwise;
  if (socketwise) {
    console.log('connected to socket.io');
    conn.join(socketwise)
    conn.on('slide_update', (contributor, updatingData) => {
      console.log("data to change in slide");
      console.log(updatingData);
      switch (updatingData.type) {
        case "add":
          switch (updatingData.added) {
            case "slide":
              DBucket
                .mutateIn(socketwise)
                .arrayAppend("slides", {
                  id: updatingData.content.id,
                  content: []
                }).execute(function (err, stats) {
                  if (err) {
                    console.log(err);
                  }else {
                    console.log("saved to database");
                  }
                })
              break;
            case "content":
              var order = updatingData.slide.replace("slide-", "");
              DBucket
                .mutateIn(socketwise)
                .arrayAppend(`slides[${order}].content`, {
                  id: updatingData.content.id,
                  type: updatingData.content.type,
                  styles: [updatingData.content.style],
                  text: "",
                  role: updatingData.content.style
                }).execute(function (err, stats) {
                  if (err) {
                    console.log(err);
                  }
                  else {
                    console.log(stats);
                  }
                })
              break;
            default:

          }
          break;
        case "update":
          var sOrder = updatingData.element.origin.replace("slide-", "");
          var cOrder = updatingData.element.content;
          DBucket
            .mutateIn(socketwise)
            .upsert(`slides[${sOrder}].content[${cOrder}].text`, updatingData.text)
            .execute(function (err, stats) {
              if (err) {
                console.log(err);
              }else {
                console.log(stats);
              }
            })
          break;
        case "style":
        var sOrder = updatingData.element.origin.replace("slide-", "");
        var cOrder = updatingData.element.content;
        DBucket
          .mutateIn(socketwise)
          .upsert(`slides[${sOrder}].content[${cOrder}].style`, [updatingData.style])
          .execute(function (err, stats) {
            if (err) {
              console.log(err);
            }else {
              console.log(stats);
            }
          })
          break;
      }
      io.to(socketwise).emit('slide_update', contributor, updatingData)
      console.log(contributor);
      console.log('sent through opened connection');
      console.log(socketwise);
    })
  }
})
// ---------- EXPRESS:ROUTE ---------- \\
app.get('/', (req, res) => {
  var user = null;
  var msg = "";
  var reason = req.query.reason;
  var emailLeftover = req.query.id;
  var nameLeftover = req.query.name;
  var blockfor = "nothing";
  var regerr = false;
  var logerr = false;
  var svrerr = false;
  if (reason) {
    switch (reason) {
      case "servererror":
      case "loggedout":
      case "forbidden":
      case "missingpage":
        blockfor = "error";
        svrerr = true;
        break;
      case "registererror":
        blockfor = "register";
        regerr = true;
        break;
      case "oautherror":
        blockfor = "login";
        logerr = true;
        break;
    }
    switch (reason) {
      case "servererror": msg = "Server Error"; break;
      case "loggedout": msg = "Logged Out"; break;
      case "forbidden": msg = "Forbidden"; break;
      case "missingpage": msg = "Broken Link"; break;
      case "registererror":
      case "oautherror": msg = "Please Try Again"; break;
    }
  }
  if (req.session.passport) {
    user = req.session.passport.user;
  }
  res.render('intro', {err: {block: blockfor, problem: reason, log: logerr, reg: regerr, svr: svrerr, msg: msg, leftover: {email: emailLeftover, name: nameLeftover}}, user: user})
})
app.get('/twitter', passport.authenticate('twitter'));
app.get('/oauth/twitter', passport.authenticate('twitter', { successRedirect: '/docs',
                                     failureRedirect: '/?reason=oautherror' }));
app.get('/facebook', passport.authenticate('facebook'));
app.get('/oauth/facebook',passport.authenticate('facebook', {successRedirect: '/docs',
                                     failureRedirect: '/?reason=oautherror' }));
app.post('/secureidentification', parser.json(), (req, res) => { //login portal
  // NOT IN USE
})
app.get('/logout', function(req, res){
  req.logout();
  res.redirect('/?reason=loggedout');
});
app.post('/whosyourfriendthatlikestoplay', parser.json(), (req, res) => { //register portal
  // NOT IN USE
})
app.post('/credentiallookup', parser.json(), (req, res) => { // credential lookup
  // NOT IN USE
  if (req.body.pass) {
    UBucket
      .lookupIn(req.body.email)
      .get('password')
      .execute(function(err, res) {
        var pass = res.content('password');
        pass.sort((a, b) => a.time - b.time)
        console.log(pass[0].value);
      });
  }else {
    users.get(req.body.email, function(err, stats) {
      if (err) {
          if (err.code == couchbase.errors.keyNotFound) {
            res.send(JSON.stringify({exists: false}))
          } else {
            res.send(JSON.stringify({error: err, msg: "please contact admin"}))
          }
      } else {
        res.send(JSON.stringify({exists: true}))
      }
    });
  }

});
app.get('/docs', (req, res) => {
  PassportSession.get();
  var passport = req.session.passport;
  try {
    documents(passport.user.email, function (docs) {
      res.render('manager', {user: {name: passport.user.name, email: passport.user.email, avatar: passport.user.avatar}, docs: docs})
    })
  } catch (e) {
    res.redirect('/?reason=loggedout')
  }
})
app.get('/edit', (req, res) => {
  sendthemPage(req, res, "editor", "decorator");
})
app.get('/deco', (req, res) => {
  sendthemPage(req, res, "decorator", "decorator");
})
app.get('/pp', (req, res) => {
  // NOT RELATED TO THE PROJECT: ForceTouch Trackpad Testing Page
  res.render('pp')
})
app.get('/css/:stylesheet', (req, res) => {
  res.type("text/css").send(sass.renderSync({
    file: `stylesheets/${req.params.stylesheet}`,
    outputStyle: "compressed",
  }).css.toString());  // NOTE: not for production
})
// ----------: Production :----------\\
// app.get('/css/:stylesheet', (req, res) => {
  // res.send(csses[req.params.stylesheet])
// }

// ---------- SASS ---------- \\

// ----------: Production :----------\\
// for (var i = 0; i < scsses.length; i++) {
//   var result = sass.renderSync({
//     file: `stylesheets/${scsses[i]}`,
//     outputStyle: "compressed",
//   });
//   csses[scsses[i]] = result.css.toString();
// }
// ---------- PassportJS ---------- \\
passport.use(new TwitterStrategy({
    consumerKey: tw.consumerKey,
    consumerSecret: tw.consumerSecret,
    callbackURL: "http://localhost:8000/oauth/twitter",
    userProfileURL  : 'https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true'
  },
  function(token, tokenSecret, profile, done) {
    Users.find(profile, function (err, user, avatar) {
      if (err) {
        console.log(err);
        return done(err);
      }
      else {
        if (avatar) {
          user.avatar = avatar;
        }
        console.log(`Token:\n${token}`);
        PassportSession.set();
        return done(null, user);
      }
    });
  }
));
passport.use(new FacebookStrategy({
  clientID: fb.appid,
  clientSecret: fb.appsecret,
  callbackURL: "http://localhost:8000/oauth/facebook",
  profileFields: ['id', 'displayName', 'photos', 'email']
},
function(token, tokenSecret, profile, done) {
  Users.find(profile, function (err, user, avatar) {
    if (err) {
      console.log(err);
      return done(err);
    }
    else {
      if (avatar) {
        user.avatar = avatar;
      }
      console.log(`Token:\n${token}`);
      PassportSession.set();
      return done(null, user);
    }
  });
}))
// ---------- COUCHBASE ---------- \\
var N1qlQuery = couchbase.N1qlQuery;
var cluster = new couchbase.Cluster(process.env.CB_HOST);
cluster.authenticate(process.env.CB_ADMIN, process.env.CB_PASS);
var UBucket = cluster.openBucket('Users');
var DBucket = cluster.openBucket('Docs');
function documents(email, callback) {
  var qrystr = `SELECT name, collaborators FROM Docs WHERE owner='${email}'`;
  var query = N1qlQuery.fromString(qrystr);
  UBucket.query(query, function(err, rows, meta) {
    if (!err) {
      callback(rows);
    }else {
      console.log(err);
    }
  })
}
function getDoc(docOwner, docName, callback) {
  var qrystr = `SELECT slides, collaborators FROM Docs WHERE owner = "${docOwner}" AND name = "${docName}"`;
  var query = N1qlQuery.fromString(qrystr);
  DBucket.query(query, function(err, rows, meta) {
    if (rows.length == 0) {
      callback(err);
    }else {
      callback(err, rows[0], meta);
    }
  })
}
const Users = {
  find: function (profile, callback) {
    var qrystr = `SELECT ${profile.provider}, name, avatar, meta().id AS email FROM Users WHERE meta().id = '${profile.emails[0].value}'`;
    var query = N1qlQuery.fromString(qrystr);
    UBucket.query(query, function(err, rows, meta) {
      if (rows.length == 0) {
        _create_user(profile, callback)
      }else {
        if (rows[0][profile.provider]) {
          callback(err, rows[0], profile.photos[0].value);
        }else {
          UBucket.mutateIn(profile.emails[0].value).
          insert(profile.provider, profile.id).
          execute(function (err, res) {
            if (!err) {
              callback(err, rows[0]);
            }else {
              console.log(err);
            }
          })
        }
      }
    })
  }
}

function sendthemPage(req, res, maxPage, minPage) {
  PassportSession.get();
  var passport = req.session.passport;
  if (passport) {
    console.log(socketwise);
    var doc = req.query.document;
    var owner = req.query.owner;
    var socketwise = (req.query.socketwise)? req.query.socketwise : `${owner}:${doc}`;
    var pageAttr = {
      user: {
        name: passport.user.name,
        email: passport.user.email,
        avatar: passport.user.avatar,
      },
      socketwise: socketwise,
      doc: doc
    };
    if (req.query.new) {
      _new_document(pageAttr.user.email, doc, (slide)=>{
        pageAttr.slide = slide;
        res.render(maxPage, pageAttr);
      })
    }else if (doc && owner) {
      getDoc(owner, doc, (err, slide, meta)=>{
        if (err) {
          res.redirect('/?reason=missingpage')
        }else {
          var accessGranted = false, userRole = "";
          if (owner != passport.user.email) {
            var collabs = slide.collaborators;
            for (var role in collabs) if (collabs.hasOwnProperty(role)) {
              if (collabs[role].includes(passport.user.email)) {
                pageAttr.user.role = role;
                accessGranted = true;
                break;
              }
            }
          }else {
            accessGranted = true;
            pageAttr.user.role = "owner";
          }
          if (accessGranted) {
            pageAttr.slide = slide;
            console.log(pageAttr.slide);
            switch (pageAttr.user.role) {
              case "owner":
              case "edit":
              case "story":
                res.render(maxPage, pageAttr)
                break;
              case "decorate":
                res.render(minPage, pageAttr)
                break;
              case "comment":
              case "view":
                res.render('present', pageAttr)
            }
          }else {
            res.redirect('/?reason=forbidden')
          }
        }
      });
    }else {
      res.redirect('/docs');
    }
  }else {
    res.redirect('/?reason=loggedout')
  }
}
function _create_user(profile, callback) {
  UBucket.insert(profile.emails[0].value, {name: profile.displayName, [profile.provider]: profile.id, avatar: profile.photos[0].value, documents:[]}, (err, stats)=>{
    if (!err) {
      callback(err);
    }else {
      callback(err, {email: profile.emails[0].value, name: profile.name, avatar: profile.photos[0].value});
    }
  })
}
function _new_document(owner, doc, callback) {
  var _new_slide = {
      name: doc,
      owner: owner,
      slides: [],
      styles: [],
      theme: "dark",
      background: "",
      collaborators: {
        edit: [],
        story: [],
        decorate: [],
        comment: [],
        view: []
      },
      audience: 0
    };
  DBucket.insert(`${owner}:${doc}`, _new_slide, function (err, stats) {
      if (err) {
        console.log(err);
      }else {
        callback(_new_slide);
      }
    })
}
const PassportSession = {
  set: function () {
    passport.serializeUser(function(user, done) {
      done(null, user);
    });
  },
  get: function () {
    passport.deserializeUser(function(user, done) {
      if (user) {
        done(null, user);
      }else {
        done("user not logged in");
      }
    });
  }
}
