FROM node:13

# Create app directory
WORKDIR /usr/src/app/nonthapat

# Install app dependencies
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

EXPOSE 8000
CMD [ "node", "server.js" ]
