var win = {
  x: innerWidth,
  y: innerHeight,
  xr: innerWidth * devicePixelRatio,
  yr: innerHeight * devicePixelRatio,
  min: findMin(innerWidth * devicePixelRatio, innerHeight * devicePixelRatio),
  max: findMax(innerWidth * devicePixelRatio, innerHeight * devicePixelRatio),
  xc: innerWidth / 2,
  yc: innerHeight / 2,
  pixel: devicePixelRatio
}
function findMin(a, b){
  return (a > b)? b : a;
}
function findMax(a, b){
  return (a > b)? a : b;
}

var canvas, ctx, dotArr = [];
function set(canvas) {
  canvas.set({width: win.xr, height: win.yr});
  canvas.style.width = win.x + "px";
  canvas.style.height = win.y + "px";
}

function canvasify(selector, density) {
  canvas = the(selector);
  set(canvas);
  ctx = canvas.getContext("2d");
  window.on({resize: function () {
    set(canvas);
  }})
  var spc = win.xr/density;
  for (var i = 0; i < density * (win.yr/win.xr); i++) {
    for (var j = 0; j < density; j++) {
      var
        x = Math.round(spc * j) + Math.round(spc/2),
        y = Math.round(spc * i) + Math.round(spc/2),
        size = Math.floor(Math.random() * (spc/4)),
        speed = Math.random() * ((spc - (density/spc))/win.xr),
        color = "#ffffff",
        minsize = spc/12,
        maxsize = spc/4
      ;

      dotArr.push(new Dot(x, y, size, speed, color, minsize, maxsize));
    }
  }
  animate();
}
function animate() {
  requestAnimationFrame(animate);
  ctx.clearRect(0, 0, win.xr, win.yr);
  for (var i = 0; i < dotArr.length; i++) {
    dotArr[i].animate();
  }
}
function Dot(x, y, s, v, c, n, m) {
  this.x = x;
  this.y = y;
  this.s = (s < n)? n : s;
  this.v = (v < 0.2)? 0.2 : v;
  this.n = n;
  this.m = m;
  this.draw = function () {
    ctx.beginPath();
    ctx.fillStyle = c;
    ctx.arc(this.x, this.y, this.s, 0, Math.PI * 2);
    ctx.fill();
  }
  this.animate = function () {
    if (this.s >= this.m || this.s <= this.n) {
      this.v = -this.v;
    }
    this.s += this.v;
    this.draw();
  }
}
canvasify("#canvas", 50);
