function all(sel) {
  return document.querySelectorAll(sel);
}
NodeList.prototype.are = function (cname) {
  for (var i = 0; i < this.length; i++) {
    for (var j = 0; j < arguments.length; j++) {
      this[i].classList.add(arguments[j]);
    }
  }
  return this;
};
NodeList.prototype.arent = function (cname) {
  for (var i = 0; i < this.length; i++) {
    for (var j = 0; j < arguments.length; j++) {
      this[i].classList.remove(arguments[j]);
    }
  }
  return this;
};
NodeList.prototype.have = function (elem) {
  for (var i = 0; i < this.length; i++) {
    for (var j = 0; j < arguments.length; j++) {
      this[i].appendChild(arguments[j].cloneNode(true));
    }
  }
  return this;
};
NodeList.prototype.on = function (listeners) {
  for (var i = 0; i < this.length; i++) {
    for (var action in listeners) {
      if (listeners.hasOwnProperty(action)) {
        this[i].addEventListener(action, listeners[action]);
      }
    }
  }
  return this;
};
NodeList.prototype.ignore = function (listeners) {
  for (var i = 0; i < this.length; i++) {
    for (var action in listeners) {
      if (listeners.hasOwnProperty(action)) {
        this[i].removeEventListener(action, listeners[action]);
      }
    }
  }
  return this;
};
NodeList.prototype.clear = function () {
  for (var i = 0; i < this.length; i++) {
    while (this.childNodes.length > 0) {
      this[i].childNodes[0].remove();
    }
  }
  return this;
};
NodeList.prototype.says = function (txt) {
  for (var i = 0; i < this.length; i++) {
    this[i].appendChild(document.createTextNode(txt));
  }
  return this;
};
NodeList.prototype.txt = function (t) {
  for (var i = 0; i < this.length; i++) {
    this[i].clear().appendChild(document.createTextNode(t));
  }
  return this;
};
NodeList.prototype.set = function (attr) {
  for (var i = 0; i < this.length; i++) {
    for (var key in attr) {
      if (attr.hasOwnProperty(key)) {
        this[i].setAttribute(key, attr[key]);
      }
    }
  }
  return this;
};
Number.prototype.elements = function (tag) {
  var elems = document.createDocumentFracment();
  try {
    if (this > 1000) {
      console.log("The code demanded more than 1000 copies of the new element, this might take a while.");
    }
    for (var i = 0; i < this; i++) {
      elems.appendChild(document.createElement(tag));
    }
  } catch (e) {
    console.log(`error somewhere in ${this}.elements(${tag})`);
  }
  return elems;
};
