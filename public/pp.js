window.on({load: function (e) {
  var instr = (navigator.userAgent.match(/iPhone|Android/))? "Touch" : "Click";
  var visr = the(".touch.visualizer");
  visr.find("text").txt(instr);
  if (instr == "Click") {
    visr.on({
      webkitmouseforcechanged: function (e) {
        var force = (e.webkitForce > 1)? e.webkitForce * 0.5 : 0.5;
        this.find("button").style.backgroundColor = `hsl(${12+((force-0.5)*108)}, 100%, 50%)`;
        this.find("decoration").style.backgroundColor = `hsl(${12+((force-0.5)*108)}, 100%, 50%)`;
        this.style.transform = `scale(${0.5+force})`;
        this.find("text").txt(Math.round((force - 0.5)* 100)/100);
        if (force <= 0.5) {
          this.isnt("inflated");
          this.find("text").txt(instr);
        }else {
          this.is("inflated")
        }
      }.bind(visr),
      webkitmouseforcewillbegin: function (e) {
        e.preventDefault();
      }
    })
  }else if (instr == "Touch") {
    the(".touch.visualizer").on({
      touchstart: function (e) {
        console.log(e.force);
      }
    })
  }
}})
