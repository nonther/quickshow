window.onload = init;
function init() {
  polyfill();
  function playBanner() {
    var billboard = the(".billboard");
    if (billboard.scrollLeft == billboard.scrollWidth - innerWidth) {
      billboard.scrollTo({top: 0, left: 0, behavior: 'smooth'});
    }else {
      billboard.scrollBy({top: 0, left: innerWidth, behavior: 'smooth'});
    }
  }
  var carousel = setInterval(playBanner, 8000);
  the("#block").on({click: function (e) {
    e.target.unset("class")
  }})
  the(".billboard").on({scroll: function (e) {
    clearInterval(carousel);
    carousel = setInterval(playBanner, 8000);
  }})
  the(".kindly-login").on({click: function (e) {
    e.stopPropagation();
    the("#block").className = "login";
  }});
  the(".kindly-register").on({click: function (e) {
    e.stopPropagation();
    the("#block").className = "register";
  }});
  the("#popup-register").on({click: function (e) {
    e.stopPropagation();
  }})
  the("#owned").on({click: function (e) {
    e.stopPropagation();
    if (this.checked) {
      the("#keep").unset("disabled");
    }else {
      the("#keep").checked = false;
      the("#keep").set({disabled: true});
    }
  }.bind(the("#owned"))})
  all("button.register").on({click: function (e) {
    e.stopPropagation();
    var block = the("#block");
    block.is("register");
    block.on({click: function (e) {
      e.target.unset("class");
    }});
  }});
  all("a.login").on({click: function (e) {
    e.stopPropagation();
    var block = the("#block");
    block.className = "login";
    block.on({click: function (e) {
      e.target.unset("class");
    }});
  }});
  all(".input.text input").on({focus: function (e) {
    e.stopPropagation();
    one.above(e.target).is("inserted");
  }, blur: function (e) {
    if (e.target.value == "") {
      one.above(e.target).isnt("inserted");
    }
  }, input: function (e) {
    if (e.target.value != "") {
      one.above(e.target).is("inserted");
    }
  }});
  all("input").forEach((item, i) => {
    if (item.value != "") {
      one.above(item).is("inserted")
    }
  });

}
function lookup(email) {
  fetch("/credentiallookup",{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({email: email})
  }).then((response) => response.json()).then((result) => {
    if (result.exists) {
      console.log("The account already exists");

    }
  })
}
