function $(elem) {
  return document.getElementById(elem);
}
function connectWS() {
  return io.connect(window.location.origin, {
    query: `socketwise=${encodeURIComponent(socketwise)}`,
    'reconnection': true,
    'reconnectionDelay': 1000,
    'reconnectionDelayMax' : 5000,
    'reconnectionAttempts': 5
  });
}
function changesMade(change) {
  if (change) {
    ws.emit("slide_update", ws.id, change)
  }
  if (slideSaved) {
    slideSaved = false;
    the("#save-button").isnt("disabled").txt("SAVE").set({disabled: false});
  }
}
var ws, socketwise;
var macOS = navigator.userAgent.match(/Mac OS X/);
var mobile = navigator.userAgent.match(/iPhone|Android/);
var slideSaved = true;
var textStyle = ["Title", "Subtitle", "Heading", "Paragraph", "Caption", "Disclaimer"];
var listStyle = ["Number", "Roman", "Capital", "Alphabet", "Bullet", "Dash"];
var codeStyle = ["HTML", "JavaScript", "CSS", "SQL", "PHP", "Python", "Ruby", "Java", "Golang", "Swift", "C", "C++", "C#", "Lysp"]
var removedItems = [], recoveredItems = [];
var documentName = "";
var user = {
  email: the(".user-profile").dataset.email,
  name: the(".user-profile").dataset.name,
  id: the(".user-profile").dataset.id
};
function nthChild(cell) {
  var child = cell, i = 0;
  while( (child = child.previousElementSibling) != null ) i++;
  return i;
}
function div(classes) {
  var d = a("div");
  for (var i = 0; i < arguments.length; i++) {
    d.classList.add(arguments[i]);
  }
  return d;
}
function input(classes) {
  var inp = a("input");
  for (var i = 0; i < arguments.length; i++) {
    inp.classList.add(arguments[i]);
  }
  return inp;
}
var slideNo = 0, elemNo = 0;
function removeItem(elem, target) {
  elem.isnt("removable");
  target.isnt("removable");
  var item = {root: elem.parentElement, item: elem, before: elem.previousElementSibling, after: elem.nextElementSibling};
  removedItems.push(item);
  QsBasicRemove(elem)
  the("#recover-recently-deleted").isnt("disabled");
  changesMade({type: "remove", element: {
    wrapper: elem.id
  }});
}
function QsBasicRemove(elem) {
  elem.remove();
}
function addElem(title, action) {
  return  div("new-elem-icon").as(`qs-add-${title.toLowerCase()}`).set({title: title}).on({click: action});
}
function slide(space = "qs-working-area") {
  var innerContent = content("Title", "text", "Text");
  var elemwrap = div("slide-elements").as(`element-wrapper-${slideNo}`).has(innerContent)
  var slidewrap = div("slide").as(`slide-${slideNo}`);
  var moElm =
      div("add-element").as("qs-more-element").has(
        addElem("Text", function () {content("Paragraph", "text", "Text", this)}.bind(elemwrap)),
        addElem("List", function () {content("Bullet", "stack", "List Item", this)}.bind(elemwrap)),
        addElem("Code", function () {content("HTML", "block", "Code Here", this)}.bind(elemwrap)),
        addElem("Table", function () {content("Table", "table", "", this)}.bind(elemwrap)),
        addElem("Graph", function () {content("Graph", "link", "Query URL", this)}.bind(elemwrap)),
        addElem("Pictures", function () {content("Picture", "link", "Picture URL", this)}.bind(elemwrap)),
        addElem("Video", function () {content("Video", "link", "Video URL", this)}.bind(elemwrap))
      );
  slidewrap.has(elemwrap, moElm);
  slidewrap.on({
    keydown: function (e) {
      e.stopPropagation();
      var modKey = (macOS)? e.metaKey : e.ctrlKey;
      if (modKey){
        var isStack = is(one.above(e.target))("stack");
        var wrapper = (isStack)? one.above(one.above(e.target)) : one.above(e.target);
        removable(wrapper);
        if (e.shiftKey) {
          var container = (is(one.above(e.target))("stack"))? one.above(e.target) : this;
          removable(container);
        }
        if (e.altKey && isStack) {
          unremovable(wrapper);
          removable(e.target);
        }
        if (e.which == 8) {
          removeItem(the(".removable"), e.target);
        }
      }
    }.bind(slidewrap),
    keyup: function (e) {
      var modKey = (macOS)? e.metaKey : e.ctrlKey;
      var isStack = is(one.above(e.target))("stack");
      var wrapper = (isStack)? one.above(one.above(e.target)) : one.above(e.target);
      if (isStack && !e.altKey) {
        unremovable(e.target);
        removable(wrapper);
      }
      if (!modKey){
        unremovable(wrapper);
        unremovable(this);
      }
      if (!e.shiftKey) {
        unremovable(this);
      }
    }.bind(slidewrap)
  });
  $(space).has(slidewrap);
  moElm.has(repositioner(slidewrap));
  one.within(innerContent).focus();
  slideNo++;
  return slidewrap.id;
}
function removable(target) { //not working cannot detect modifier keys without pressing letter keys
  target.is("removable");
}
function unremovable(target) { //not working cannot detect modifier keys without pressing letter keys
  target.isnt("removable");
}
function moveUp(e) {
  var prequel = one.before(this)
  if (prequel) {
    prequel.before(this);
  }
}
function moveDown(e) {
  var sequel = one.after(this);
  if (sequel) {
    sequel.after(this);
  }
}
function repositioner(elm) {
  var repostnr = div("repositioner");
  var mvup = div("move-up").on({click: moveUp.bind(elm)}), mvdn = div("move-down").on({click: moveDown.bind(elm)});
  return repostnr.has(mvup, mvdn);
}
function updateText(e) {
  var txt = e.target.value
  changesMade({type: "update", text: txt, element: {
    origin: getOriginSlide(e.target),
    content: getElementNo(e.target)
  }})
}
function getWrapper(elem) {
  var wrapper = elem;
  while (!wrapper.classList.contains("slide-element")) {
    wrapper = one.above(wrapper);
  }
  return wrapper;
}
function getWrapperID(elem) {
  return getWrapper(elem).id;
}
function getOriginSlide(elem) {
  var origin = elem;
  while (!origin.classList.contains("slide")) {
    origin = one.above(origin);
  }
  return origin.id;
}
function getElementNo(elem) {
  var i = 0;
  var child = getWrapper(elem);
  while( (child = child.previousSibling) != null )
    i++;
  return i;
}
function keypressHandler(e) {changesMade()}
function selectElemRole(e, __etype) {
  e.stopPropagation();
  listOut(e.target, __etype, textStyle);
}
function content(contentType, classification, displayText, parentelem) {
  var ctyl = contentType.toLowerCase();
  var innerMost = null;
  var contFig = div("element-type");
  var contWrap = div("slide-element", `${ctyl}-element`);
  contWrap.dataset.role = contentType.toLowerCase();
  contWrap.dataset.type = classification;
  switch (classification) {
    case "text":
    case "link":
      innerMost = input("text").set({type: classification, placeholder: contentType}).on({keypress: keypressHandler, blur: updateText});
      contFig.is("text-style")
      if (classification == "text") {
        if (mobile) {
          mobileOption(contFig)(textStyle, contentType);
        }else {
          contFig.has(div("text-style-selected").says(contentType))
          contFig.on({click: function (e) {
            selectElemRole(e, "text")
          }});
        }
      }
      break;
    case "block":
      innerMost = a("textarea").on({keypress: function (e) {changesMade()}, blur: updateText});
      contFig.is("code-style");
      if (mobile) {
        mobileOption(contFig)(codeStyle, contentType);
      }else {
        contFig.has(div("code-style-selected").says(contentType)).on({click: function () {
          selectElemRole(e, "code")
        }});
      }
      break;
    case "stack":
      innerMost = div("stack");
      innerMost.has(div("add2stack").txt("List Item").has(a("span").says("\u23CE")).on({click: function (e) {
        add2stack.bind(this)({which: 13});
      }.bind(innerMost)}));
      add2stack.bind(innerMost)({which: 13});
      contFig.is("list-style")
      if (mobile) {
        mobileOption(contFig)(listStyle, contentType);
      }else {
        contFig.has(div("list-style-selected").says(contentType)).on({click: function (e) {
          selectElemRole(e, "list")
        }});
      }
      break;
    case "table":
      innerMost = a("table");
      contFig.is("table-sizinator").has(
        div("col-sizinator").on({click: function () { addColumn(innerMost)}}),
        div("row-sizinator").on({click: function () { addRow(innerMost, contFig)}}),
        div("detonator").has(div("row-detonator"), div("colm-detonator")),
        div("cell-selector")
      )
      addRow(innerMost, contFig);
      addRow(innerMost, contFig);
      break;
  }
  contWrap.has(contFig, innerMost);
  innerMost.as(`elem-${getElementNo(innerMost)}`).is(ctyl).set({placeholder: displayText});
  if (parentelem) {
    parentelem.has(contWrap)
    changesMade({type: "add", added: "content", content: {id: innerMost.id, style: contentType, type: classification, desc: displayText}, slide: getOriginSlide(parentelem)})
  }
  return contWrap;
}
function addRow(table, tableTools) {
  var ins = table.children.length == 0;
  var tag = (ins)? "th" : "td";
  var colms = (ins)? 2 : table.firstElementChild.children.length;
  var row = a("tr");
  for (var i = 0; i < colms; i++) {
    row.has(a(tag).has(input("text").set({type: "text"}).on({keypress: function (e) {changesMade()}, keydown: navigateTable.bind(table)})));
    if (ins) {
      var colDetonator = div("detonate");
      tableTools.find("colm-detonator").has(
        colDetonator.on({click: detonateColm.bind({table: table, i: colDetonator})})
      );
    }
  }
  tableTools.find("row-detonator").has(
    div("detonate").on({click: detonateRow.bind(row)})
  );
  table.has(row);
}
function addColumn(table) {
  var row = table.children;
  for (var i = 0; i < row.length; i++) {
    var tag = (i == 0)? "th" : "td";
    row[i].has(a(tag).has(input("text").set({type: "text"}).on({keypress: function (e) {changesMade()}, keydown: navigateTable.bind(table)})));
  }
  var colDetonator = div("detonate");
  table.parentElement.find("colm-detonator").has(
    colDetonator.on({click: detonateColm.bind({table: table, i: colDetonator})})
  );
}
function detonateRow(e) {
  if (confirm("This action cannot be undone: Remove the row?")) {
    this.remove();
    e.target.remove();
  }
}
function detonateColm(e) {
  if (confirm("This action cannot be undone: Remove the column?")) {
    var colNo = nthChild(this.i);
    for (var i = 0; i < this.table.children.length; i++) {
      this.table.children[i].children[colNo].remove();
    }
    e.target.remove();
  }
}
function detonateItm(e) {
  removeItem(this, this);
}
function navigateTable(e) {
  var cell = e.target.parentElement;
  var row = cell.parentElement;
  var i = nthChild(cell);
  switch (e.key) {
    case "ArrowUp": if (row.previousElementSibling)
      row.previousElementSibling.children[i].firstElementChild.focus();
      break;
    case "ArrowDown": if (row.nextElementSibling)
      row.nextElementSibling.children[i].firstElementChild.focus();
      break;
    case "ArrowLeft": if (e.target.selectionStart == 0)
      if (cell.previousElementSibling){
        e.preventDefault();
        cell.previousElementSibling.firstElementChild.focus();
      }
      break;
    case "ArrowRight": if (e.target.selectionStart == e.target.value.length)
      if (cell.nextElementSibling){
        e.preventDefault();
        cell.nextElementSibling.firstElementChild.focus();
      }
      break;
  }
}
function add2stack(e) {
  if (e.which == 13) {
    var listItem = div("list-item");
    listItem.has(
      input().set({
        type: "text", placeholder: "List Item"
      }).on({
        keypress: function (e) {changesMade()},
        keyup: add2stack.bind(this)
      }),
      div("detonate").on({
        click: detonateItm.bind(listItem)
      })
    )
    this.lastElementChild.before(listItem);
    listItem.focus();
  }
}
function listOut(wrapper, compType, listList) {
  var oldV = wrapper.textContent, oldVl = wrapper.textContent.toLowerCase();
  window.on({click: closeCXmenu})
  var cxmenu = div("element-type", "open");
  var bound = wrapper.getBoundingClientRect();
  cxmenu.style.left = `${bound.x}px`;
  for (var i = 0; i < listList.length; i++) {
    var txs = listList[i], txsl = listList[i].toLowerCase();
    var opt = div(`${compType}-style-item`, txsl, "open").says(txs);
    if (oldVl == txsl) {
      opt.is("selected");
      cxmenu.style.top = `calc(${bound.y}px - ${(Math.round(i*155)/100)+0.4}em)`;
    }
    cxmenu.has(opt).on({mousemove: scrollCX});
    opt.on({click: function (e) {
      var sltds = e.target.textContent, sltdsl = e.target.textContent.toLowerCase();
      var wpr = getWrapper(this);
      var tip = wpr.the("input");
      e.stopPropagation();
      this.clear();
      this.says(sltds);
      tip.isnt(oldVl).is(sltdsl);
      wpr.isnt(`${oldVl}-element`).is(`${sltdsl}-element`);
      wpr.dataset.role = sltdsl;
      closeCXmenu()
      changesMade({type: "style", element: {origin: getOriginSlide(this), content: getElementNo(this)}, style: sltdsl})
    }.bind(wrapper),
    mousemove: scrollCX});
  }
  document.body.has(cxmenu);
}
function mobileOption(wrapper) {
  return function (list, defaultV) {
    var selector = a("select");
    for (var item in list) {
      if (list.hasOwnProperty(item)) {
        var opt = a("option").txt(list[item]).set({value: list[item].toLowerCase()});
        selector.has(opt);
        selector.on({change: function (e) {
          let wrap = one.above(one.above(e.target)), inp = one.after(one.above(e.target));
          let selected = e.target.options[e.target.selectedIndex];
          inp.isnt(wrap.dataset.role).is(selected.value);
          wrap.isnt(`${wrap.dataset.role}-element`).is(`${selected.value}-element`);
          wrap.dataset.role = selected.value;
        }})
        if (list[item] == defaultV) {
          opt.selected = true;
        }
      }
    }
    this.has(selector);
  }.bind(wrapper);
}
function scrollCX(e) {
  e.stopPropagation();
  var cx = e.target.parentElement;
  if ((e.movementY < 0 && cx.offsetTop < 0) || (cx.offsetTop + cx.offsetHeight > innerHeight && e.movementY > 0)) {
    cx.style.top = `${cx.offsetTop - e.movementY}px`;
  }
}
function closeCXmenu() {
  try{
    var opn = the(".open").isnt("open");
    opn.remove();
  }catch(err){}
  window.ignore({click: closeCXmenu});
}
function more2stack(e) {
  if (e.which == 13) {
    var innerMost = input().set({type: this.type, placeholder: this.placeholder});
    innerMost.on({keypress: more2stack.bind(innerMost)});
    one.above(this).has(innerMost);
  }
}
function promptDocumentName(button) {
  var firstHeader = the("input").value;
  var docName = input("doc-name").as("doc-name-prompt").set({
    type: "text",
    placeholder: "Untitled Slide",
    value: firstHeader
  }).on({dblclick: function (e) {
    e.target.value = "Untitled Slide";
  }});
  var blocker = div("blocker");
  var saver = a("button").is("default").says("save").on({click: function (e) {
    documentName = this.d.value;
    this.x.remove();
    save(this.b);
  }.bind({d: docName, b: button, x: blocker})});
  if (firstHeader == "" || firstHeader == undefined) {
    saver.set({disabled: true});
  }
  docName.on({keyup: function (e) {
    if (e.which == 13) {
      documentName = this.d.value;
      this.x.remove();
      save(this.b);
    }
    if (e.target.value != "") {
      saver.disabled = false;
    }else {
      saver.disabled = true;
    }
  }.bind({d: docName, b: button, x: blocker, s: saver})});
  blocker.has(
    div("document-name", "prompt").has(
      div("notice").has(
        a("h1").says("You haven't named your work!"),
        a("p").says("We can't save documents without its name.")
      ),
      a("label").set({for: "doc-name-prompt"}).says("Document Name: "),
      docName,
      saver,
      a("button").says("cancel").on({click: function (e) {
        this.remove();
      }.bind(blocker)})
    )
  );
  document.body.has(blocker);
}
function save(button) {
  if (documentName == "") {
    promptDocumentName(button);
  }else {
    the("#branding .logo").is("loading");
    var data = {doc: documentName, owner: user, changes: []};
    var space = the("#qs-working-area").children;
    for (var i in space) if (space.hasOwnProperty(i)) { // slide
      var slide = space[i].firstElementChild;
      var slides = slide.children;
      var elements = {slide: space[i].id, elements: []}
      for (var elem in slides) if (slides.hasOwnProperty(elem)) { // element
        var obj = {
          type: slides[elem].dataset.type,
          role: slides[elem].dataset.role,
          content: null,
          styling: null
        };
        switch (true) {
          case slides[elem].dataset.type == "text":
          case slides[elem].dataset.type == "link":
          case slides[elem].dataset.type == "block":
            obj.content = slides[elem].lastElementChild.value;
            obj.styling = slides[elem].firstElementChild.dataset.styling;
            break;
          case slides[elem].dataset.type == "stack":
            var stack = slides[elem].lastElementChild.children;
            var stk = []
            for (var itm in stack) if(stack.hasOwnProperty(itm)) {
              if (!is(stack[itm])("add2stack")) {
                stk.push(stack[itm].firstElementChild.value);
              }
            }
            obj.content = stk;
            break;
          case slides[elem].dataset.type == "table":
            var table = slides[elem].lastElementChild.children;
            var tbl = [];
            for (var num in table) if (table.hasOwnProperty(num)) {
              var row = table[num].children;
              var orow = {row: num, cell: []};
              for (var cell in row) if (row.hasOwnProperty(cell)) {
                orow.cell.push({cell: cell, text: row[cell].firstElementChild.value});
              }
              tbl.push(orow);
            }
            obj.content = tbl;
        }
        elements.elements.push(obj)
      }
      data.changes.push(elements);
    }
    fetch('/sync', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data)
    }).then((response) => response.json())
    .then((data) => {
      setTimeout(function () {
        the("#branding .logo").isnt("loading");
      }, 1000);
      console.log(`Task Completed`);
      console.log(data);
    });
    slideSaved = true;
    button.is("disabled").txt("SAVED").set({disabled: true});
  }
}
the("#qs-more-slide").on({click: function () {
  var slideid = slide();
  changesMade({type: "add", added: "slide", content: {id: slideid}});
}})
function recoverItem() {
  if (removedItems.length > 0) {
    var recoveredItem = removedItems.pop();
    if (recoveredItem.before) {
      recoveredItem.before.after(recoveredItem.item);
    }else if (recoveredItem.after) {
      recoveredItem.after.before(recoveredItem.item);
    }else if (recoveredItem.root) {
      recoveredItem.root.has(recoveredItem.item);
    }
    changesMade();
    recoveredItems.push(recoveredItem);
  }
  if (removedItems.length < 1) {
    this.is("disabled");
  }
}
window.on({load: function () {
  socketwise = the("#doc-info").dataset.socketwise;
  ws = connectWS();
  ws.on('connection', (socket) => {
    console.log('a user connected');
  });
  ws.on('connect', () => {
    the("header #connection").txt("")
  });
  ws.on('disconnect', () => {
    the("header #connection").txt("Disconnected")
  });
  ws.on('reconnecting', (tries) => {
    console.log(tries);
    the("header #connection").txt("Trying to reconnect")
  });
  ws.on('reconnect_failed', () => {
    the("header #connection").txt("No Internet.").has(
      a("a").set({href: "#"}).txt("Retry").on({click: function () {
        ws = connectWS();
      }})
    )
  });
  ws.on('reconnect', () => {
    the("header #connection").txt("")
  });
  ws.on("slide_update", (contributor, changes) => {
    if (contributor != ws.id) {
      console.log(`Incoming update from ${contributor}`);
      switch (changes.type) {
        case "update":
          $(changes.element.content).value = changes.text;
          break;
        case "add":
          switch (changes.added) {
            case "slide": slide();
              break;
            case "content":
              $(changes.slide).has(content(changes.content.style, changes.content.type, changes.content.desc));
              break;
          }
          break;
        case "move":

          break;
        case "remove":
          QsBasicRemove($(changes.element.wrapper))
          break;
      }
    }
  })
  slideNo=$("qs-working-area").children.length;
  var undo = the("#recover-recently-deleted");
  documentName = the("#doc-nav > input").value;
  undo.on({click: recoverItem.bind(undo)})
  the("nav a#next-button").set({href: `/deco?owner=${encodeURIComponent(user.email)}&document=${encodeURIComponent(documentName)}`});
  if (macOS) {
    document.on({keydown: function (e) {
      if (e.metaKey) {
        e.preventDefault();
        switch (e.key) {
          case "z":
            recoverItem.bind(this)();
            break;
          case "n":
            slide();
            changesMade();
            break;
          default:
        }
      }
    }.bind(undo)});
  }else {
    document.on({keydown: function (e) {
      if (e.ctrlKey) {
        e.preventDefault();
        switch (e.key) {
          case "z":
            recoverItem.bind(this)();
            break;
          case "n":
            slide();
            changesMade();
            break;
          default:
        }
      }
    }.bind(undo)});
  }
}, touchstart: function (e) {
  console.log(e.changedTouches);
}, touchmove: function (e) {
  console.log(e.changedTouches);
}})
