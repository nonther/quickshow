var userEmail = "";
window.onload = function () {
  userEmail = encodeURIComponent(the("#qs-user-email").dataset.email);
  the("#qs-user-email").remove();
  the("#new-doc").on({click: function (e) {
    var slideName = a("input").set({placeholder: "Untitled"}).on({
      keyup: function (e) {
        if (e.target.value != "") {
          the(".popup .choices .default").isnt("disabled");
          the("#new-document-destination").set({href: `/edit?owner=${userEmail}&document=${encodeURIComponent(e.target.value)}&new=true`})
          if (e.key == "Enter") {
            the(".popup .choices .default").click();
          }
        }else {
          the(".popup .choices .default").is("disabled");
          the("#new-document-destination").set({href: "#"})
        }

      }
    });
    document.body.has(a("div").is("popup").has(
      a("h1").txt("Show Name: "),
      slideName,
      a("div").is("choices").has(
        a("a").is("default", "disabled").as("new-document-destination").txt("create"),
        a("a").txt("cancel").on({click: function (e) {
          the("body > .popup").remove();
        }})
      )
    ))
    slideName.focus();
  }})
}
