# QuickShow

In this document, is the instructions on how to deploy QuickShow application. There are two main ways to deploy QuickShow on your system. 

1. Deploy using Docker (recommended)
2. Deploy using Git (not tested)

#### Repositories

- [BitBucket (git)](https://bitbucket.org/nonther/quickshow/src/master/)
- [Docker Hub (containerized)](https://hub.docker.com/repository/docker/nonther/quickshow)

## Getting Started

To deploy QuickShow, you'll need [Couchbase Server](https://www.couchbase.com/downloads?family=couchbase-server) Community or Enterprise Edition for database. See [how to setup database](#configure-database) for detail on database configuration.

[Docker](https://www.docker.com/products/docker-desktop) (optional) for containerized version

### Install and Run (using container)

#### Prepare Docker

Download [Docker Desktop](https://www.docker.com/products/docker-desktop) and complete setup process

#### Build, Deploy & Run Database

**Build Database**

Using command line on `db` directory provided, run the following command

```bash
docker build -t quickshow-db .
```

**Deploy & Run**

After Docker finished building the container, run this following command

```bash
docker run -d -p 8091-8093:8091-8093 -e CB_ADMIN=Administrator -e CB_PASS=Password00 -e CBB_USER_= -e CBB_DOCS_= --network='bridge' --name qs-couchbase-1 quickshow-db
```

Wait about 30 seconds to 1 minute for the commands to be executed, and then use your browser, go to `localhost:8091`, you should see Couchbase login page. If you did not see login page, please refer to [Troubleshooting Section](#troubleshooting).

Use the following command to get Couchbase container IP address

```bash
docker inspect qs-couchbase-1
```

then copy the value on `IPAddress` field 

#### Deploy & Run Main App

```bash
docker run -dp 8000:8000 -e CB_HOST=http://172.17.0.2:8091 -e CB_ADMIN=Administrator -e CB_PASS=Password00 nonther/quickshow
```

If the IP address we saw earlier on the last step isn't `172.17.0.2` please change it to the IP address you saw, for example: `CB_HOST=http://172.16.0.2:8091`

### Install and Run (without container)

#### Prepare App Environment

Install Node.js from one of these sources

**NVM**:

```bash
nvm install 13
```

**Homebrew**:

```bash
brew install node
```

To install nvm, see [nvm documentation](http://nvm.sh/)

If you don't have any package manager listed above, see how to [download Node.js](https://nodejs.org/en/download/package-manager/) with package manager on your system.

*Note*: Using Node.js verion 13.14 is recommended to avoid compatibility issues, but latest version should work fine.

#### Prepare Couchbase Server

Download [Couchbase Server](https://www.couchbase.com/downloads?family=couchbase-server) Community or Enterprise Edition and install to your system. Once finished, refer to [Configure Database](#standalone-couchbase-server) to setup couchbase for this project. 

#### Deploy

Clone repository

```bash
git clone git@bitbucket.org:nonther/quickshow.git
```

Install dependencies

```bash
npm install
```

#### Run

```bash
node .
```

*Note*: make sure to pass the environment variables with the run command, otherwise the app won't function correctly. Please lookup `how to pass environment variables` on the internet, because the solution is different from system to system. 

*excuse for not providing the solution on this document*: Deploying application using this method is not the main focus of this document.

## Configure Database

### Standalone Couchbase Server

Go to `localhost:8091` and click `Setup New Cluster`

Input your desired cluster name, user name, and password.

Click `Next: Accept Terms`, check `I accept the terms & conditions`, click `Configure Disk, Memory, Services`, then adjust the values to your desiring or use default settings, then click `Save & Finish`

*Note: This setup document is based on Couchbase Community Edition. If you have Enterprise Edition, the process might be different.*

Continue the steps below.

### Containerized Couchbase Server (Docker)

Go to `localhost:8091` using your web browser, then login using your credential. Default values that is documented on the `Install and Run (using container)` section are `Administrator` and `Password00`

On `Query` tab, run these query commands by copying each of them and paste on `Query Editor` and click `Execute`. 

```sql
CREATE PRIMARY INDEX ON Users
```

```sql
CREATE INDEX doc_owner ON Docs(owner) USING GSI
```

If `Query Result` returns error message, please refer to [Troubleshooting Section](troubleshooting).

## Troubleshooting

This section provided solutions to common errors that might happen during installation. I hope you don't get to use it, but just in case.

### Docker

#### Unable to prepare context 

Docker was unable to find `Dockerfile` in the current directory. It is possible that you are working in the wrong directory or `Dockerfile` is stored in the wrong directory.

**Solution**

1. Change working directory
   - Locate `Dockerfile` in file system using `Finder` or `File Explorer`.
   - On macOS, drag folder icon title bar to Terminal, hit return.
   - On Windows, use `cd` command on Command Prompt window to locate destination directory.

2. Move `Dockerfile` to current directory
3. If no `Dockerfile` provided, create one.
   - for main app, refer to [Main App Dockerfile](#main-app-dockerfile).
   - for database, refer to [Couchbase Dockerfile](#couchbase-dockerfile).

#### Invalid Reference Format: Repository Name Must Be Lowercase

There is an error in your command line syntax while calling `docker run`. 

**Solution**

1. Run again, and make sure you didn't miss `-e` flag, no `$`, and image name and container name must be all lowercase.

#### "docker build" requires exactly 1 argument.

There's a chance that you miss the `.` (dot) at the end to tell docker where the `Dockerfile` is located (current directory). It should be in the same directory as other project files.

**Solution**

1. Check that your command conform to the following pattern

   ```bash
   docker build -t image-name .
   ```

### Couchbase

#### Please Set Indexer Storage Mode, GSI CreateIndex() - cause: Fails to create index

Couchbase Indexer Storage Mode is not set. There's a chance that the provided `configure.sh` contains faulty code, because the Community Edition does not come with memory_optimized mode.

**Solution**

1. Rebuild Couchbase with new configuration file

   - in `configure.sh` change `storageMode=memory_optimized` to `storageMode=forestdb`.

   - remove database container by using the following command

     ```bash
     docker rm -f qs-couchbase-1
     ```

   - Rebuild by referring to [Build, Deploy & Run Database](#build-deploy-run-database) on Install and Run (using container) section.

#### Not Seeing Login Page

If you use container to run Couchbase, there's a chance that you passed the wrong environment variable to the container, or the code in `configure.sh` was executed before Couchbase finished setting up. 

**Solution**

1. Make sure you pass all environment variable correctly
   - remove container by using the command below, and run again with the correct variable as follow:
     - `CB_ADMIN` for Couchbase Admin Username
     - `CB_PASS` for Couchbase Admin Password
     - `CBB_USER_` for `Users` bucket password (leave blank)
     - `CBB_DOCS_` for `Docs` bucket password (leave blank)

2. Configure Couchbase Server using Web UI

   - Refer to [Configure Database using Standalone Couchbase Server](#standalone-couchbase-server)

3. Rebuild Couchbase Container

   - remove database container by using the following command

     ```bash
     docker rm -f qs-couchbase-1
     ```

   - Open `configure.sh` and change sleep time to higher value, save.
   - Rebuild by referring to [Build, Deploy & Run Database](#build-deploy-run-database) on Install and Run (using container) section.

#### cannot perform operations on a shutdown bucket

The database is not running, or the bucket was not created. 

**Solution**

1. Make sure that the database was turned on.
   - If using Docker, open Dashboard on Docker Desktop and make sure that qs-couchbase-1 has green icon. 
     - If not, try cliking on start or restart button. You should see the icons when you move your cursor over it.  
     - If the problem still persisted, click on it, read the log, and resolve the problem accordingly
   - If using standalone application, go to `http://localhost:8091` you should see Couchbase login UI. 
     - If not, launch Couchbase Server on your computer. 
     - If you can't find it, there's a chance that you didn't install Couchbase, so go ahead and grab one at [Couchbase Official Website](https://www.couchbase.com/downloads?family=couchbase-server) or build one with Docker (the instruction is provided in this document)

## Redundant Information

#### configure.sh

```sh
set -m

/entrypoint.sh couchbase-server &

sleep 30

curl -v -X POST http://127.0.0.1:8091/pools/default -d memoryQuota=512 -d indexMemoryQuota=512

curl -v http://127.0.0.1:8091/node/controller/setupServices -d services=kv%2cn1ql%2Cindex

curl -v http://127.0.0.1:8091/settings/web -d port=8091 -d username=$CB_ADMIN -d password=$CB_PASS

curl -i -u $CB_ADMIN:$CB_PASS -X POST http://127.0.0.1:8091/settings/indexes -d 'storageMode=forestdb'

curl -v -u $CB_ADMIN:$CB_PASS -X POST http://127.0.0.1:8091/pools/default/buckets -d name=Users -d bucketType=couchbase -d ramQuotaMB=128 -d authType=sasl -d saslPassword=$CBB_USER_

sleep 5

curl -v -u $CB_ADMIN:$CB_PASS -X POST http://127.0.0.1:8091/pools/default/buckets -d name=Docs -d bucketType=couchbase -d ramQuotaMB=128 -d authType=sasl -d saslPassword=$CBB_DOCS_

fg 1

```

#### db/Dockerfile

Dockerfile for building database

```dockerfile
FROM couchbase:community

COPY configure.sh /opt/couchbase

CMD ["/opt/couchbase/configure.sh"]
```

#### Dockerfile

Dockerfile for main app

```dockerfile
FROM node:13

# Create app directory
WORKDIR /usr/src/app/nonthapat

# Install app dependencies
COPY package*.json ./
RUN npm install

# Bundle app source
COPY . .

EXPOSE 8000
CMD [ "node", "server.js" ]
```

#### Project Structure

```
source
├── db
⎪   ├── Dockerfile
⎪   └── configure.sh
├── public
⎪   ├── assets
⎪   ⎪   ├── black
⎪   ⎪   ⎪   ├── codeblock.png
⎪   ⎪   ⎪   ├── graph.png
⎪   ⎪   ⎪   ├── list.png
⎪   ⎪   ⎪   ├── picture.png
⎪   ⎪   ⎪   ├── present.png
⎪   ⎪   ⎪   ├── share.png
⎪   ⎪   ⎪   ├── table.png
⎪   ⎪   ⎪   ├── text.png
⎪   ⎪   ⎪   └── video.png
⎪   ⎪   ├── hd
⎪   ⎪   ⎪   ├── 401.svg
⎪   ⎪   ⎪   ├── 403.svg
⎪   ⎪   ⎪   ├── facebook.svg
⎪   ⎪   ⎪   └── twitter.svg
⎪   ⎪   ├── wacky-promo
⎪   ⎪   ⎪   └── duo-card.png
⎪   ⎪   ├── white
⎪   ⎪   ⎪   ├── codeblock.png
⎪   ⎪   ⎪   ├── graph.png
⎪   ⎪   ⎪   ├── list.png
⎪   ⎪   ⎪   ├── picture.png
⎪   ⎪   ⎪   ├── present.png
⎪   ⎪   ⎪   ├── share.png
⎪   ⎪   ⎪   ├── table.png
⎪   ⎪   ⎪   ├── text.png
⎪   ⎪   ⎪   └── video.png
⎪   ⎪   ├── 1pass.png
⎪   ⎪   ├── dashl.png
⎪   ⎪   ├── folder.png
⎪   ⎪   ├── icon-r.png
⎪   ⎪   ├── lastp.png
⎪   ⎪   ├── folder.svg
⎪   ⎪   ├── icon-r.svg
⎪   ⎪   └── logo.svg
⎪   ├── canvas.html
⎪   ├── canvasify.js
⎪   ├── editor.js
⎪   ├── header.js
⎪   ├── intro.js
⎪   ├── library.js
⎪   ├── single.js
⎪   └── smoothscroll.js
├── stylesheets
⎪   ├── decorator.sass
⎪   ├── editor.sass
⎪   ├── header.sass
⎪   ├── intro.sass
⎪   └── library.sass
├── views
⎪   ├── snippets
⎪   ⎪.  └── header.pug
⎪   ├── decorator.pug
⎪   ├── editor.pug
⎪   ├── intro.pug
⎪   └── manager.pug
├── .gitignore
├── .dockerignore
├── Dockerfile
├── server.js
├── package-lock.json
└── package.json
```
